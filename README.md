# Clustering/Merging Tags with Wordcloud Tool

This tool can give you UI to merge the similar tags and dumps to database.

## Prerequisites

Python >= 2.7


## Installation

`pip install Django`


## Run

`python manage.py runserver`


## Sample screenshots

### #1
Landing page
![Sample #1](/docs/1.png)

### #2
Choose a file
![Sample #2](/docs/2.png)

### #3
Ploting wordcloud from file uploaded
![Sample #3](/docs/3.png)

### #4
Select some tags
![Sample #4](/docs/4.png)

### #5
Unselect some tags and select some more tags
![Sample #5](/docs/5.png)

### #6
Merge to the desired category
![Sample #6](/docs/6.png)


## Limitations

- At present., allows only .csv and .txt files (tested).
- Supporting browsers., chrome and firefox (tested).
- Dumping to database, yet to code.


## Credits

[D3 wordcloud example](https://bl.ocks.org/jyucsiro/767539a876836e920e38bc80d2031ba7)