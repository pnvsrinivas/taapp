from __future__ import unicode_literals

from django.conf.urls import url, include

from . import views

urlpatterns = [
    url(r'^$', views.D3WordCloudView.as_view(), name='d3'),
    url(r'^home/$', views.Index.as_view(), name='index'),
    url(r'^anychart/$', views.AnyChartWordCloudView.as_view(), name='anychart'),
    
]