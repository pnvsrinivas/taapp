# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views import generic

class Index(generic.View):
    def get(self, request):
        context = {}
        return render(request, "index.html", context)


class AnyChartWordCloudView(generic.View):
    def get(self, request):
        context = {}
        return render(request, "anychart.html", context)

class D3WordCloudView(generic.View):
    def get(self, request):
        context = {}
        return render(request, "d3.html", context)
