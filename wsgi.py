# -*- coding:utf-8 -*-
########################################################
# CONFIDENTIAL
# __________________
#
# © Copyright ICUBE GLOBAL, LLC.  2014 -2015. All Rights Reserved
#
# NOTICE:  All information contained herein is, and remains
# the property of ICUBE GLOBAL, LLC.
# The intellectual and technical concepts contained
# herein are proprietary to ICUBE GLOBAL, LLC.
# and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright
# Law. Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from ICUBE GLOBAL, LLC.
######################################################
"""
WSGI config for intuCEO project.

This module contains the WSGI application used by Django's development server
and any production WSGI deployments. It should expose a module-level variable
named ``application``. Django's ``runserver`` and ``runfcgi`` commands discover
this application via the ``WSGI_APPLICATION`` setting.

Usually you will have the standard Django WSGI application here, but it also
might make sense to replace the whole Django WSGI application with a custom one
that later delegates to the Django one. For example, you could introduce WSGI
middleware here, or combine a Django application with an application of another
framework.

"""
import os
import sys
sys.path.append('/var/www/taapp')
sys.path.append('/var/www/taapp/TA')

# We defer to a DJANGO_SETTINGS_MODULE already in the environment. This breaks
# if running multiple sites in the same mod_wsgi process. To fix this, use
# mod_wsgi daemon mode with each site in its own daemon process, or use
# os.environ["DJANGO_SETTINGS_MODULE"] = "intuCEO.settings"
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "TA.settings")

# This application object is used by any WSGI server configured to use this
# file. This includes Django's development server, if the WSGI_APPLICATION
# setting points here.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

# Apply WSGI middleware here.
# from helloworld.wsgi import HelloWorldApplication
# application = HelloWorldApplication(application)
